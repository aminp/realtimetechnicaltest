package com.realtime.technical_test.repository;

import com.realtime.technical_test.model.Permission;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

/**
 * Created by amin.purnawinandar on 9/20/2016.
 */
@Repository
public class PermissionRepositoryImpl implements PermissionRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void save(Permission permission) throws DataAccessException {
        if (permission.getId() == null) {
            this.entityManager.persist(permission);
        } else {
            this.entityManager.merge(permission);
        }
    }

    @Override
    public Collection<Permission> findAll() throws DataAccessException {
        return entityManager.createQuery("from Permission").getResultList();
    }

    @Override
    public Collection<Permission> findByName(String name) throws DataAccessException {
        return (Collection<Permission>) entityManager.createQuery(
                "from Permission where name = :name")
                .setParameter("name", name).getResultList();
    }

    @Override
    public Permission findById(Integer id) throws DataAccessException {
        return (Permission) entityManager.createQuery(
                "from Permission where id = :id")
                .setParameter("id", id).getSingleResult();
    }

    @Override
    public void delete(Permission model) throws DataAccessException {
        if (entityManager.contains(model)) {
            entityManager.remove(model);
        } else {
            entityManager.remove(entityManager.merge(model));
        }
        return;
    }
}
