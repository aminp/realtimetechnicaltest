package com.realtime.technical_test.repository;

import com.realtime.technical_test.model.Employee;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

/**
 * Created by amin.purnawinandar on 9/20/2016.
 */
@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Collection<Employee> findByName(String name) throws DataAccessException {
        return (Collection<Employee>) entityManager.createQuery(
                "from Employee where first_name = :first_name")
                .setParameter("first_name", name).getResultList();
    }

    @Override
    public Employee findById(Integer id) throws DataAccessException {
        return (Employee) entityManager.createQuery(
                "from Employee where id = :id")
                .setParameter("id", id).getSingleResult();
    }

    @Override
    public Collection<Employee> findByLastName(String lastName) throws DataAccessException {
        return (Collection<Employee>) entityManager.createQuery(
                "from Employee where last_name = :last_name")
                .setParameter("last_name", lastName).getResultList();
    }

    @Override
    public void save(Employee employee) throws DataAccessException {
        if (employee.getId() == null) {
            this.entityManager.persist(employee);
        } else {
            this.entityManager.merge(employee);
        }
    }

    @Override
    public void delete(Employee employee) throws DataAccessException {
        if (entityManager.contains(employee)) {
            entityManager.remove(employee);
        } else {
            entityManager.remove(entityManager.merge(employee));
        }
        return;
    }

    @Override
    public Collection<Employee> findAll() throws DataAccessException {
        return entityManager.createQuery("from Employee").getResultList();
    }
}
