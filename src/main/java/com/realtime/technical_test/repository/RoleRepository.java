package com.realtime.technical_test.repository;

import com.realtime.technical_test.model.Role;

/**
 * Created by amin.purnawinandar on 9/20/2016.
 */
public interface RoleRepository extends BaseRepository<Role, Integer> {

}
