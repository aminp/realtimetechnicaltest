package com.realtime.technical_test.repository;

import com.realtime.technical_test.model.Permission;

/**
 * Created by amin.purnawinandar on 9/20/2016.
 */
public interface PermissionRepository extends BaseRepository<Permission, Integer> {

}
