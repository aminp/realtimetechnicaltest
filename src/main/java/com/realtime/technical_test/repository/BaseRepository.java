package com.realtime.technical_test.repository;

import com.realtime.technical_test.model.BaseEntity;
import org.springframework.dao.DataAccessException;

import java.util.Collection;

/**
 * Created by amin.purnawinandar on 9/22/2016.
 */
public interface BaseRepository<E extends BaseEntity, T> {
    Collection<E> findAll() throws DataAccessException;

    Collection<E> findByName(String name) throws DataAccessException;

    E findById(T id) throws DataAccessException;

    void save(E model) throws DataAccessException;

    void delete(E model) throws DataAccessException;
}
