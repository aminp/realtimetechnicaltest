package com.realtime.technical_test.repository;

import com.realtime.technical_test.model.Role;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

/**
 * Created by amin.purnawinandar on 9/20/2016.
 */
@Repository
public class RoleRepositoryImp implements RoleRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Role findById(Integer id) throws DataAccessException {
        return (Role) entityManager.createQuery(
                "from Permission where id = :id")
                .setParameter("id", id).getSingleResult();
    }

    @Override
    public void delete(Role model) throws DataAccessException {
        if (entityManager.contains(model)) {
            entityManager.remove(model);
        } else {
            entityManager.remove(entityManager.merge(model));
        }
        return;
    }

    @Override
    public Collection<Role> findByName(String name) throws DataAccessException {
        return (Collection<Role>) entityManager.createQuery(
                "from Role where name = :name")
                .setParameter("name", name).getResultList();
    }

    @Override
    public void save(Role role) throws DataAccessException {
        if (role.getId() == null) {
            this.entityManager.persist(role);
        } else {
            this.entityManager.merge(role);
        }
    }

    @Override
    public Collection<Role> findAll() throws DataAccessException {
        return entityManager.createQuery("from Role").getResultList();
    }
}
