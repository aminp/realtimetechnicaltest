package com.realtime.technical_test.repository;

import com.realtime.technical_test.model.Employee;
import org.springframework.dao.DataAccessException;

import java.util.Collection;

/**
 * Created by amin.purnawinandar on 9/20/2016.
 */
public interface EmployeeRepository extends BaseRepository<Employee, Integer> {
    /**
     * Retrieve <code>Employee</code>s from the data store by last name, returning all employees whose last name <i>starts</i>
     * with the given name.
     *
     * @param lastName Value to search for
     * @return a <code>Collection</code> of matching <code>Employee</code>s (or an empty <code>Collection</code> if none
     * found)
     */
    Collection<Employee> findByLastName(String lastName) throws DataAccessException;
}
