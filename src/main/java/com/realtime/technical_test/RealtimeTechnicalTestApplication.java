package com.realtime.technical_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@ComponentScan("com.realtime")
//@ImportResource("classpath:spring-config1.xml")
public class RealtimeTechnicalTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(RealtimeTechnicalTestApplication.class, args);
	}
}
