package com.realtime.technical_test.service;

import com.realtime.technical_test.model.Employee;

/**
 * Created by amin.purnawinandar on 9/22/2016.
 */
public interface EmployeeService extends BaseService<Employee, Integer> {

}
