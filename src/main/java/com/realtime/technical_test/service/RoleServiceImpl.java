package com.realtime.technical_test.service;

import com.realtime.technical_test.model.Role;
import com.realtime.technical_test.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * Created by amin.purnawinandar on 9/22/2016.
 */
@Service
public class RoleServiceImpl implements RoleService {
    private RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<Role> findAll() {
        return roleRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<Role> findByName(String name) {
        return roleRepository.findByName(name);
    }

    @Override
    @Transactional(readOnly = true)
    public Role findById(Integer id) {
        return roleRepository.findById(id);
    }

    @Override
    @Transactional
    public void save(Role model) {
        roleRepository.save(model);
    }

    @Override
    @Transactional
    public void delete(Role model) {
        roleRepository.delete(model);
    }
}
