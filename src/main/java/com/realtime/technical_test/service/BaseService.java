package com.realtime.technical_test.service;

import com.realtime.technical_test.model.BaseEntity;

import java.util.Collection;

/**
 * Created by amin.purnawinandar on 9/22/2016.
 */
public interface BaseService<E extends BaseEntity, T> {
    Collection<E> findAll();
    Collection<E> findByName(String name);
    E findById(T id);
    void save(E model);
    void delete(E model);
}
