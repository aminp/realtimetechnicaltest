package com.realtime.technical_test.service;

import com.realtime.technical_test.model.Permission;
import com.realtime.technical_test.repository.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * Created by amin.purnawinandar on 9/22/2016.
 */
@Service
public class PermissionServiceImpl implements PermissionService {
    private PermissionRepository permissionRepository;

    @Autowired
    public PermissionServiceImpl(PermissionRepository permissionRepository) {
        this.permissionRepository = permissionRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<Permission> findAll() {
        return permissionRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<Permission> findByName(String name) {
        return permissionRepository.findByName(name);
    }

    @Override
    @Transactional(readOnly = true)
    public Permission findById(Integer id) {
        return permissionRepository.findById(id);
    }

    @Override
    @Transactional
    public void save(Permission model) {
        permissionRepository.save(model);
    }

    @Override
    @Transactional
    public void delete(Permission model) {
        permissionRepository.delete(model);
    }
}
