package com.realtime.technical_test.service;

import com.realtime.technical_test.model.Role;

/**
 * Created by amin.purnawinandar on 9/22/2016.
 */
public interface RoleService extends BaseService<Role, Integer> {
}
