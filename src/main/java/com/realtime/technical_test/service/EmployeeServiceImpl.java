package com.realtime.technical_test.service;

import com.realtime.technical_test.model.Employee;
import com.realtime.technical_test.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * Created by amin.purnawinandar on 9/22/2016.
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<Employee> findAll() {
        return employeeRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<Employee> findByName(String name) {
        return employeeRepository.findByName(name);
    }

    @Override
    @Transactional(readOnly = true)
    public Employee findById(Integer id) {
        return employeeRepository.findById(id);
    }

    @Override
    @Transactional
    public void save(Employee model) {
        employeeRepository.save(model);
    }

    @Override
    @Transactional
    public void delete(Employee model) {
        employeeRepository.delete(model);
    }
}
