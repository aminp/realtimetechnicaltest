package com.realtime.technical_test.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by amin.purnawinandar on 9/20/2016.
 */
@Entity
@Table(name = "role")
public class Role extends NamedEntity {
    //@OneToOne(fetch = FetchType.EAGER)
    //@JoinColumn(name = "role_id")
    @OneToOne(mappedBy = "role")
    private Employee employee;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "role_permission", joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id"))
    private Set<Permission> permissions;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Set<Permission> getPermissions() {
        if (this.permissions == null) {
            this.permissions = new HashSet<Permission>();
        }
        return permissions;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public void addPermission(Permission permission) {
        this.getPermissions().add(permission);
    }
}
