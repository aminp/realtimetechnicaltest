package com.realtime.technical_test.model;

import javax.persistence.*;

/**
 * Created by amin.purnawinandar on 9/20/2016.
 */
@Entity
@Table(name = "employee")
public class Employee extends Person {

//    @OneToOne(fetch = FetchType.EAGER, mappedBy = "employee")
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    private Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
