package com.realtime.technical_test.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * Created by amin.purnawinandar on 9/20/2016.
 */
@MappedSuperclass
public class NamedEntity extends BaseEntity {

    @Column(name = "name")
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.getName();
    }

}