package com.realtime.technical_test.controller;

import com.realtime.technical_test.model.Permission;
import com.realtime.technical_test.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Collection;

/**
 * Created by amin.purnawinandar on 9/22/2016.
 */
@Controller
public class PermissionController {

    private PermissionService permissionService;

    @Autowired
    public PermissionController(PermissionService permissionService) {
        this.permissionService = permissionService;
    }

    @RequestMapping(value = "/permission", method = RequestMethod.GET)
    public Collection<Permission> findAll() {
        return permissionService.findAll();
    }

    @RequestMapping(value = "/permission/create", method = RequestMethod.POST)
    public String create(Permission permission) {
        try {
            permissionService.save(permission);
        } catch (Exception ex) {
            return "Error creating the permission: " + ex.toString();
        }
        return "Permission succesfully created!";
    }

    @RequestMapping(value = "/permission/{perm_id}/update", method = RequestMethod.POST)
    public String update(Permission permission, @PathVariable("perm_id") int permId) {
        try {
            permission.setId(Integer.valueOf(permId));
            permissionService.save(permission);
        } catch (Exception ex) {
            return "Error updating the permission: " + ex.toString();
        }
        return "Permission succesfully updated!";
    }

    @RequestMapping(value = "/permission/{perm_id}/delete", method = RequestMethod.POST)
    public String delete(Permission permission, @PathVariable("perm_id") int permId) {
        try {
            permission.setId(Integer.valueOf(permId));
            permissionService.delete(permission);
        } catch (Exception ex) {
            return "Error deleting the permission: " + ex.toString();
        }
        return "Permission succesfully deleted!";
    }
}
