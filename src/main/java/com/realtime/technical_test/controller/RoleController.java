package com.realtime.technical_test.controller;

import com.realtime.technical_test.model.Role;
import com.realtime.technical_test.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Collection;

/**
 * Created by amin.purnawinandar on 9/22/2016.
 */
@Controller
public class RoleController {
    private RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @RequestMapping(value = "/role", method = RequestMethod.GET)
    public Collection<Role> findAll() {
        return roleService.findAll();
    }

    @RequestMapping(value = "/role/create", method = RequestMethod.POST)
    public String create(Role role) {
        try {
            roleService.save(role);
        } catch (Exception ex) {
            return "Error creating the role: " + ex.toString();
        }
        return "Role succesfully created!";
    }

    @RequestMapping(value = "/role/{role_id}/update", method = RequestMethod.POST)
    public String update(Role role, @PathVariable("role_id") int roleId) {
        try {
            role.setId(Integer.valueOf(roleId));
            roleService.save(role);
        } catch (Exception ex) {
            return "Error updating the role: " + ex.toString();
        }
        return "Role succesfully updated!";
    }

    @RequestMapping(value = "/role/{role_id}/delete", method = RequestMethod.POST)
    public String delete(Role role, @PathVariable("role_id") int roleId) {
        try {
            role.setId(Integer.valueOf(roleId));
            roleService.delete(role);
        } catch (Exception ex) {
            return "Error deleting the role: " + ex.toString();
        }
        return "Role succesfully deleted!";
    }
}
