package com.realtime.technical_test.controller;

import com.realtime.technical_test.model.Employee;
import com.realtime.technical_test.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Collection;

/**
 * Created by amin.purnawinandar on 9/22/2016.
 */
@Controller
public class EmployeeController {
    private EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = this.employeeService;
    }

    @RequestMapping(value = "/employee/{employee_id}", method = RequestMethod.GET)
    public Employee findById(@PathVariable("employee_id") int employeeId) {
        return employeeService.findById(Integer.valueOf(employeeId));
    }

    @RequestMapping(value = "/employee", method = RequestMethod.GET)
    public Collection<Employee> findAll() {
        return employeeService.findAll();
    }

    @RequestMapping(value = "/employee/create", method = RequestMethod.POST)
    public String create(Employee employee) {
        try {
            employeeService.save(employee);
        } catch (Exception ex) {
            return "Error creating the employee: " + ex.toString();
        }
        return "Employee succesfully created!";
    }

    @RequestMapping(value = "/employee/{employee_id}/update", method = RequestMethod.POST)
    public String update(Employee employee, @PathVariable("employee_id") int employeeId) {
        try {
            employee.setId(Integer.valueOf(employeeId));
            employeeService.save(employee);
        } catch (Exception ex) {
            return "Error updating the employee: " + ex.toString();
        }
        return "Employee succesfully updated!";
    }

    @RequestMapping(value = "/employee/{employee_id}/delete", method = RequestMethod.POST)
    public String delete(Employee employee, @PathVariable("employee_id") int employeeId) {
        try {
            employee.setId(Integer.valueOf(employeeId));
            employeeService.delete(employee);
        } catch (Exception ex) {
            return "Error deleting the employee: " + ex.toString();
        }
        return "Employee succesfully deleted!";
    }
}
