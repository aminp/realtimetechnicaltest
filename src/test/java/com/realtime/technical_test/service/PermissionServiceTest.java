package com.realtime.technical_test.service;

import com.realtime.technical_test.model.Permission;

import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by amin.purnawinandar on 9/22/2016.
 */
public class PermissionServiceTest extends BaseServiceTest<PermissionService>  {

    @Override
    public void insertModel() {
        Collection<Permission> permissions = this.service.findAll();
        int found = permissions.size();

        Permission permission = new Permission();
        permission.setName("Payroll");
        service.save(permission);
        assertThat(permission.getId().longValue()).isNotEqualTo(0);

        permissions = this.service.findAll();
        assertThat(permissions.size()).isEqualTo(found + 1);
    }
}
