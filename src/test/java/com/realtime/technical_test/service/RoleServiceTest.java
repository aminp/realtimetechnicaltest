package com.realtime.technical_test.service;

import com.realtime.technical_test.model.Role;

import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by amin.purnawinandar on 9/22/2016.
 */
public class RoleServiceTest extends BaseServiceTest<RoleService> {
    @Override
    public void insertModel() {
        Collection<Role> roles = this.service.findAll();
        int found = roles.size();

        Role role = new Role();
        role.setName("HR Manager");
        service.save(role);
        assertThat(role.getId().longValue()).isNotEqualTo(0);

        roles = this.service.findAll();
        assertThat(roles.size()).isEqualTo(found + 1);
    }
}
