package com.realtime.technical_test.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by amin.purnawinandar on 9/22/2016.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public abstract class BaseServiceTest<E extends BaseService> {
    @Autowired
    protected E service;

    @Test
    @Transactional
    public abstract void insertModel();
}
