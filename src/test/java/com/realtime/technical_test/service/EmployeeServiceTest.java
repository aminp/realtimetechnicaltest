package com.realtime.technical_test.service;

import com.realtime.technical_test.model.Employee;
import com.realtime.technical_test.model.Role;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by amin.purnawinandar on 9/22/2016.
 */
public class EmployeeServiceTest extends BaseServiceTest<EmployeeService> {

    @Autowired
    protected RoleService roleService;

    @Override
    public void insertModel() {

        List<Role> roles = new ArrayList<Role>(this.roleService.findAll());
        Role role = (Role) roles.get(0);

        Collection<Employee> permissions = this.service.findAll();
        int foundEmployee = permissions.size();


        Employee employee = new Employee();
        employee.setFirstName("Amin");
        employee.setLastName("Purnawinandar");
        employee.setRole(role);
        service.save(employee);
        assertThat(employee.getId().longValue()).isNotEqualTo(0);

        permissions = this.service.findAll();
        assertThat(permissions.size()).isEqualTo(foundEmployee + 1);
    }
}
