package com.realtime.technical_test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RealtimeTechnicalTestApplicationTests {

	@Test
	public void contextLoads() {
	}

}
